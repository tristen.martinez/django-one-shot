from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView, CreateView, DeleteView
from todos.models import TodoItem, TodoList
# Create your views here.


class TodoListListView(ListView):
    model = TodoList
    template_name = "todos/todolist.html"
    context_object_name = "banana"

class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/create_todolist.html"
    fields = ["name"]
    
    # success_url = reverse_lazy("todo_list")
    def get_success_url(self):
        return reverse_lazy("detail_todolist", args=[self.object.pk])

class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/todolist_detail.html"

class TodoListUpdate(UpdateView):
    model = TodoList
    template_name = "todos/edit_todolist.html"
    fields = ["name"]

    # success_url = reverse_lazy("detail_todolist")
    def get_success_url(self):
        return reverse_lazy("detail_todolist", args=[self.object.pk])  
        #this is to grab the pk for the new name 


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete_todolist.html"
    success_url = reverse_lazy("todo_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todos/todoitem_create.html"
    fields = ["task", "due_date", "is_completed", "list" ]

    success_url = reverse_lazy("todo_list")

class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todos/todoitem_edit.html"
    fields = ["task", "due_date", "is_completed", "list" ]

    success_url = reverse_lazy("todo_list")
