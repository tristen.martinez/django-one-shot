from django.urls import path
from todos.views import (
    TodoListListView, 
    TodoListCreateView,
    TodoListDetailView, 
    TodoListUpdate,
    TodoListDeleteView,
    TodoItemCreateView,
    TodoItemUpdateView,
)

urlpatterns = [
    path('', TodoListListView.as_view(), name="todo_list"),
    path('create/', TodoListCreateView.as_view(), name="create_todolist"),
    path("<int:pk>/", TodoListDetailView.as_view(), name="detail_todolist"),
    path("<int:pk>/edit/", TodoListUpdate.as_view(), name="edit_todolist"),
    path("<int:pk>/delete/", TodoListDeleteView.as_view(), name="delete_todolist"),

    path("items/create/", TodoItemCreateView.as_view(), name="todoitem_create"),
    path("items/<int:pk>/edit/", TodoItemUpdateView.as_view(), name="todoitem_edit",)
]